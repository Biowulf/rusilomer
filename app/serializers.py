# -*- coding: utf-8 -*-
from rest_framework import serializers

from app.models import *
from django.contrib.auth.models import User

class ExerciseSerialize(serializers.ModelSerializer):
    class Meta:
        model = Exercise
        fields = ('id','title','points')


class UserSerialize(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username')

class RegionSerialize(serializers.ModelSerializer):
    pass


class CitySerialize(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ('id','title','timezone')

class LocationSerialize(serializers.ModelSerializer):
    city = CitySerialize()
    class Meta:
        model = Location
        fields = ('id','title','city')

class CompetitionSerialize(serializers.ModelSerializer):
    location = LocationSerialize()
    date = serializers.DateField()
    class Meta:
        model = Competition
        fields = ('id','date','is_finish','location')

class ParticipantStat(serializers.ModelSerializer):
    # todo: не отображать в сериализации записи с visible==False
    def create(self, validated_data):
        participant = Participant(**validated_data)
        try:
            participant.save()
        except:
            participant.delete()

        return participant

    class Meta:
        model = PersonalStat
        fields = ('id', 'exercise', 'time')

class ParticipantSerialize(serializers.ModelSerializer):
    stats = ParticipantStat(many=True,required=False)
    first_name = serializers.CharField(max_length=20,required=False)
    second_name = serializers.CharField(max_length=20,required=False)

    def update(self, instance, validated_data):
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.second_name = validated_data.get('second_name', instance.second_name)
        instance.status = validated_data.get('status', instance.status)
        instance.points = validated_data.get('points', instance.points)
        instance.save()

        return instance

    class Meta:
        model = Participant
        fields = ('id','first_name','second_name','stats','status','points')

class GroupSerialize(serializers.ModelSerializer):
    # participants=ParticipantSerialize(many=True)
    class Meta:
        model = Group
        # fields = ('id', 'title','participants')
        fields = ('id', 'title')

class ParticipantStatusSerialize(serializers.ModelSerializer):
    # todo: не отображать в сериализации записи с visible==False
    class Meta:
        model = ParticipantStatus
        fields = ('title', 'active')

class ParticipantListSerialize(serializers.ModelSerializer):
    group = GroupSerialize()
    status = ParticipantStatusSerialize()

    class Meta:
        model = Participant
        fields = ('id', 'first_name', 'second_name', 'status', 'points', 'group', 'stats')

class CompetitionAllSerialize(serializers.ModelSerializer):
    location = LocationSerialize()
    date = serializers.DateField()
    groups = GroupSerialize(many=True)

    class Meta:
        model = Competition
        fields = ('id','date','location','groups')


class PersonalStatSerialize(serializers.ModelSerializer):
    class Meta:
        model = PersonalStat
        fields = ('participant',
                  'exercise',
                  'time',
                  'operator')

    def create(self, validated_data):
        return PersonalStat.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.participant = validated_data.get('participant', instance.participant)
        instance.exercise = validated_data.get('exercise', instance.exercise)
        instance.time = validated_data.get('time', instance.time)
        instance.operator = validated_data.get('operator', instance.operator)
        instance.save()
        return instance
# -*- coding: utf-8 -*-
"""rusilomer URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from app.views import *


urlpatterns = [
    url(r'^exercise$', ExerciseView.as_view()),
    url(r'^competition$', CompetitionView.as_view()),
    url(r'^participant$', ParticipantView.as_view()),
    # url(r'^personal/stat/add/$', PersonalStatAddView.as_view()),
    url(r'^personal/stat/addArray/$', PersonalStatAddArrayView.as_view()),
#     url(r'^personal/stat/error/$', PersonalStatErrorView.as_view()),
#     url(r'^personal/stat/del/$', PersonalStatDelView.as_view()),
#     url(r'^personal/stat/edit/$', PersonalStatEditView.as_view()),
#     url(r'^personal/stat/save/$', PersonalStatSaveView.as_view()),
]
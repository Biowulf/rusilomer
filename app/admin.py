from django.contrib import admin
from .models import *

admin.site.register(Region)
admin.site.register(City)
admin.site.register(Location)
admin.site.register(Group)
admin.site.register(ParticipantStatus) #Статус участника
admin.site.register(Participant) # Участники
admin.site.register(Exercise) # Упражнения
admin.site.register(PersonalStat) # Личная статистика
admin.site.register(Competition)

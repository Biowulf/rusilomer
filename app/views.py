from django.http import HttpResponse
from django.db.models import Q

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer
from rest_framework import status

from app.models import *
from app.serializers import *

class JSONResponse(HttpResponse):
    """
    Учим HttpResponse отдавать контент в JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        #kwargs['content_type'] = 'application/json'
        kwargs['content_type'] = 'application/vnd.api+json'

        super(JSONResponse, self).__init__(content, **kwargs)


class ExerciseView(APIView):
    '''Упражнения'''
    def get(self, request):
        try:
            exercise_list = Exercise.objects.all()
            serializer = ExerciseSerialize(exercise_list, many=True)

        except Exception as e:
            result = 'Error: {}'.format(e)
            status_code = status.HTTP_400_BAD_REQUEST

        else:
            result = serializer.data
            status_code = status.HTTP_200_OK

        return JSONResponse({"result": result }, status=status_code)


class RegionView(APIView):
    pass


class CityView(APIView):
    pass


class CompetitionView(APIView):
    '''Соревнования'''
    def get(self, request):
        try:
            select_location = Competition.objects.filter(Q(operators=request.user.pk))
            serializer = CompetitionSerialize(select_location, many=True)

        except Competition.DoesNotExist:
            result = 'Ошибка (Competition): Соревнований не найдено или у вас нет доступа!'
            status_code = status.HTTP_400_BAD_REQUEST

        except Exception as e:
            result = 'Error: {}'.format(e)
            status_code = status.HTTP_400_BAD_REQUEST

        else:
            result = serializer.data
            status_code = status.HTTP_200_OK

        return JSONResponse({"result": result}, status=status_code)


class ParticipantView(APIView):
    '''Участники'''
    def get(self, request):
        try:
            if 'id' in request.GET:
                participant_id = request.GET.get('id')
                response_data = Participant.objects.get(Q(id=participant_id) &
                                                        Q(group__competition__operators=request.user.pk))
                serializer = ParticipantSerialize(response_data)

            elif 'competition' in request.GET:
                competition_id = request.GET.get('competition')
                competition_data = Participant.objects.filter(Q(group__competition=competition_id) &
                                                           Q(group__competition__operators__id=request.user.pk))
                serializer = ParticipantListSerialize(competition_data, many=True)

        except Participant.DoesNotExist:
            result = 'Ошибка (Participant): Участник не найден или у вас нет доступа!'
            status_code = status.HTTP_400_BAD_REQUEST

        except Competition.DoesNotExist:
            result = 'Ошибка (Competition): Соревнование не найдено или у вас нет доступа!'
            status_code = status.HTTP_400_BAD_REQUEST

        except Exception as e:
            result = 'Error: {}'.format(e)
            status_code = status.HTTP_400_BAD_REQUEST

        else:
            result = serializer.data
            status_code = status.HTTP_200_OK

        return JSONResponse({"result": result}, status=status_code)


class PersonalStatAddView(APIView):
    parser_classes = (JSONParser,)
    def post(self, request):
        try:
            participant = Participant.objects.get(Q(id=request.data['participant_id']) &
                                                  Q(group__competition__operators=request.user.pk))
            if not participant.status.active:
                raise ValueError('Участник уже завершил зачёт, редактирование запрещено!')
            exercise = Exercise.objects.get(id=request.data['exercise_id'])
            serializer = PersonalStatSerialize(data={'participant': participant.id,
                                                     'exercise': exercise.id,
                                                     'operator': request.user.id})
            if not serializer.is_valid():
                raise ValueError('Ошибка сериализации: {}'.format(serializer.errors))

            serializer.save()

        except Participant.DoesNotExist:
            result = 'Ошибка (Participant): Участник не найден или у вас нет доступа!'
            status_code = status.HTTP_400_BAD_REQUEST

        except Exercise.DoesNotExist:
            result = 'Ошибка (Exercise): Упражнение c таким id не найдено!'
            status_code = status.HTTP_400_BAD_REQUEST

        except Exception as e:
            result = 'Error: {}'.format(e)
            status_code = status.HTTP_400_BAD_REQUEST

        else:
            result = serializer.data
            status_code = status.HTTP_200_OK

        return JSONResponse({"data": request.data, "result":result}, status=status_code)


class PersonalStatAddArrayView(APIView):
    """
    Массовое создание записей персональной статистики
    """
    parser_classes = (JSONParser,)

    def post(self, request, *args, **kwargs):
        try:
            participant = Participant.objects.get(Q(id=request.data['participant_id']) &
                                                  Q(group__competition__operators=request.user.pk))

            for stat in request.data['stats']:
                stat.update({'participant': participant.id,
                             'operator': request.user.id})

            serializer = PersonalStatSerialize(data=request.data['stats'], many=isinstance(request.data['stats'], list))
            serializer.is_valid(raise_exception=True)
            serializer.save()

        except Participant.DoesNotExist:
            result = 'Ошибка (Participant): Участник не найден или у вас нет доступа!'
            status_code = status.HTTP_400_BAD_REQUEST

        except Exercise.DoesNotExist:
            result = 'Ошибка (Exercise): Упражнение c таким id не найдено!'
            status_code = status.HTTP_400_BAD_REQUEST

        except Exception as e:
            result = 'Error: {}'.format(e)
            status_code = status.HTTP_400_BAD_REQUEST

        else:
            result = serializer.data
            status_code = status.HTTP_200_OK

        return JSONResponse({"data": request.data, "result": result}, status=status_code)


class PersonalStatErrorView(APIView):
    """
    Помечаем последнее упражнение как отмеченное с ошибкой 
    """
    parser_classes = (JSONParser,)
    def post(self, request):
        try:
            participant = Participant.objects.get(Q(id=request.data['participant_id']) &
                                                  Q(group__competition__operators=request.user.pk))
            if not participant.status.active:
                raise ValueError('Участник уже завершил зачёт, редактирование запрещено!')
            # Последнее упражнение из зачёта
            error_stat = PersonalStat.objects.filter(participant=participant.id).latest('id')

            serializer = PersonalStatSerialize(error_stat, data={'is_correct': False}, partial=True)
            if not serializer.is_valid():
                raise ValueError('Ошибка сериализации: {}'.format(serializer.errors))

            serializer.save()

        except Participant.DoesNotExist:
            result = 'Ошибка (Participant): Участник не найден или у вас нет доступа!'
            status_code = status.HTTP_400_BAD_REQUEST

        except PersonalStat.DoesNotExist:
            result = 'Ошибка (PersonalStat): Запись о выполнении упраженеия не найдена!'
            status_code = status.HTTP_400_BAD_REQUEST

        except Exercise.DoesNotExist:
            result = 'Ошибка (Exercise): Упражнение c таким id не найдено!'
            status_code = status.HTTP_400_BAD_REQUEST

        except Exception as e:
            result = 'Error: {}'.format(e)
            status_code = status.HTTP_400_BAD_REQUEST

        else:
            result = serializer.data
            status_code = status.HTTP_200_OK

        return JSONResponse({"data": request.data, "result": result}, status=status_code)


class PersonalStatDelView(APIView):
    parser_classes = (JSONParser,)
    def post(self, request):
        try:
            participant = Participant.objects.get(Q(id=request.data['participant_id']) &
                                                  Q(group__competition__operators=request.user.pk))
            if not participant.status.active:
                raise ValueError('Участник уже завершил зачёт, редактирование запрещено!')
            del_stat = PersonalStat.objects.get(participant=participant.id, id=request.data['stat_id'])
            del_stat_id = del_stat.id

            serializer = PersonalStatSerialize(del_stat, data={'visible': False}, partial=True)
            if not serializer.is_valid():
                raise ValueError('Ошибка сериализации: {}'.format(serializer.errors))

            serializer.save()
            print(serializer)

        except Participant.DoesNotExist:
            result = 'Ошибка (Participant): Участник не найден или у вас нет доступа!'
            status_code = status.HTTP_400_BAD_REQUEST

        except PersonalStat.DoesNotExist:
            result = 'Ошибка (PersonalStat): Запись о выполнении упраженеия не найдена!'
            status_code = status.HTTP_400_BAD_REQUEST

        except Exception as e:
            result = 'Serialize error: {}'.format(e)
            status_code = status.HTTP_400_BAD_REQUEST

        else:
            result = del_stat_id
            status_code = status.HTTP_200_OK

        return JSONResponse({"data": request.data, "result":result}, status=status_code)


class PersonalStatEditView(APIView):
    parser_classes = (JSONParser,)
    def post(self, request):
        try:
            participant = Participant.objects.get(Q(id=request.data['participant_id']) &
                                                  Q(group__competition__operators=request.user.pk))
            if not participant.status.active:
                raise ValueError('Участник уже завершил зачёт, редактирование запрещено!')
            # упражнение из зачёта для редактирования
            original_stat = PersonalStat.objects.get(participant=participant.id,id=request.data['stat_id'])
            new_exercise = Exercise.objects.get(id=request.data['new_exercise_id'])

            # 1. Копируем оригинальную запись и делаем её неотображаемой
            original_stat.pk = None
            original_stat.visible = False
            original_stat.save()

            # 2. Сохраняем оригинал с изменёнными данными
            edit_stat = PersonalStat.objects.get(participant=participant.id,id=request.data['stat_id'])
            serializer = PersonalStatSerialize(edit_stat, data={'exercise': new_exercise.id,
                                                                'is_correct': True,
                                                                'visible': True}, partial=True)

            if not serializer.is_valid():
                raise ValueError('Ошибка сериализации: {}'.format(serializer.errors))

            serializer.save()

        except Participant.DoesNotExist:
            result = 'Ошибка (Participant): Участник не найден или у вас нет доступа!'
            status_code = status.HTTP_400_BAD_REQUEST

        except PersonalStat.DoesNotExist:
            result = 'Ошибка (PersonalStat): Запись о выполнении упраженеия не найдена!'
            status_code = status.HTTP_400_BAD_REQUEST

        except Exercise.DoesNotExist:
            result = 'Ошибка (Exercise): Упражнение c таким id не найдено!'
            status_code = status.HTTP_400_BAD_REQUEST

        except Exception as e:
            result = 'Error: {}'.format(e)
            status_code = status.HTTP_400_BAD_REQUEST

        else:
            result = serializer.data
            status_code = status.HTTP_200_OK

        return JSONResponse({"data": request.data, "result": result}, status=status_code)


class PersonalStatSaveView(APIView):
    parser_classes = (JSONParser,)
    def post(self, request):
        try:
            participant = Participant.objects.get(Q(id=request.data['participant_id']) &
                                                  Q(group__competition__operators=request.user.pk))
            error_list = list(participant.stats.filter(Q(is_correct=False) &
                                                       ~Q(is_correct=False, visible=False)).values_list('id', flat=True))

            if not participant.status.active:
                raise ValueError('Участник уже завершил зачёт, редактирование запрещено!')
            elif error_list:
                result = 'У участника есть упражнения помеченные как ошибочные'
                status_code = status.HTTP_400_BAD_REQUEST
                return JSONResponse({"data": request.data, "result": result, 'error_list':error_list}, status=status_code)

            participant_stat = Exercise.objects.filter(personalstat__participant=participant).\
                values_list('points', flat=True)

            serializer = ParticipantSerialize(participant, data={'status': ParticipantStatus.objects.get(id=2).id,
                                                                 'points': sum(participant_stat)}, partial=True)
            if not serializer.is_valid():
                raise ValueError('Ошибка сериализации: {}'.format(serializer.errors))

            serializer.save()

        except Participant.DoesNotExist:
            result = 'Ошибка (Participant): Участник не найден или у вас нет доступа!'
            status_code = status.HTTP_400_BAD_REQUEST

        except PersonalStat.DoesNotExist:
            result = 'Ошибка (PersonalStat): Запись о выполнении упраженеия не найдена!'
            status_code = status.HTTP_400_BAD_REQUEST

        except Exercise.DoesNotExist:
            result = 'Ошибка (Exercise): Упражнение c таким id не найдено!'
            status_code = status.HTTP_400_BAD_REQUEST

        except Exception as e:
            result = 'Error: {}'.format(e)
            status_code = status.HTTP_400_BAD_REQUEST

        else:
            result = serializer.data
            status_code = status.HTTP_200_OK

        return JSONResponse({"data": request.data, "result": result}, status=status_code)
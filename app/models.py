import datetime
from django.db import models
from django.contrib.auth.models import User


class Region(models.Model):
    class Meta:
        verbose_name = 'Регион (Region)'
        verbose_name_plural = 'Список регионов (Region)'

    title = models.CharField(max_length=40, verbose_name='Регион')

    def __str__(self):
        return '{}'.format(self.title)


class City(models.Model):
    class Meta:
        verbose_name = 'Город (City)'
        verbose_name_plural = 'Список городов (City)'

    # todo: добавить временную зону
    region = models.ForeignKey(Region, verbose_name='Регион')
    title = models.CharField(max_length=40, verbose_name='Название города')
    timezone = models.CharField(max_length=3, verbose_name='TimeZone', blank=True, null=True)
    lat = models.FloatField(verbose_name='Широта', blank=True, null=True)
    lng = models.FloatField(verbose_name='Долгота', blank=True, null=True)
    population = models.IntegerField(verbose_name='Популяция', blank=True, null=True)

    def __str__(self):
        return '{} - {}'.format(self.title, self.region)


# Локации
class Location(models.Model):
    class Meta:
        verbose_name = 'Локация (Location)'
        verbose_name_plural = 'Список локаций (Location)'

    title = models.CharField(max_length=40,verbose_name='Описание')
    city = models.ForeignKey(City, verbose_name='Город')
    lat = models.FloatField(verbose_name='Широта', blank=True, null=True)
    lng = models.FloatField(verbose_name='Долгота', blank=True, null=True)

    def __str__(self):
        return '{} - {}'.format(self.title, self.city.title)


# Соревнование
class Competition(models.Model):
    class Meta:
        verbose_name = 'Соревнование (Competition)'
        verbose_name_plural = 'Соревнования (Competition)'

    location = models.ForeignKey(Location, verbose_name='Место проведения', on_delete=models.CASCADE)
    date = models.DateField(verbose_name='Дата проведения')
    operators = models.ManyToManyField(User, verbose_name='Секунданты (операторы)')
    is_finish = models.BooleanField(default=False, verbose_name='Завершено?')

    def __str__(self):
        return '{} | {} - {}'.format(self.location.title, self.location.city.title ,self.date)


# Группы соревнующихся (классы, комманды, и.т.д.)
class Group(models.Model):
    class Meta:
        verbose_name = 'Группа (Group)'
        verbose_name_plural = 'Список групп (Group)'

    title = models.CharField(max_length=40, verbose_name='Название')
    competition = models.ForeignKey(Competition,related_name='groups',verbose_name='Мероприятие')

    def __str__(self):
        return '{} ({})'.format(self.title, self.competition)

class ParticipantStatus(models.Model):
    class Meta:
        verbose_name = 'Статус участника (ParticipantStatus)'
        verbose_name_plural = 'Статуы участников (ParticipantStatus)'

    title = models.CharField(max_length=40, verbose_name='Описание')
    active = models.BooleanField(default=False, verbose_name='Возможность участия')

    def __str__(self):
        return '{}'.format(self.title)

# Участники
class Participant(models.Model):
    class Meta:
        verbose_name = 'Участник (Participant)'
        verbose_name_plural = 'Список участников (Participant)'

    first_name = models.CharField(max_length=20, verbose_name='Имя')
    second_name = models.CharField(max_length=20, verbose_name='Фамилия')
    group = models.ForeignKey(Group,related_name='participants', verbose_name='Группа участника')
    status = models.ForeignKey(ParticipantStatus, default=1, verbose_name='Статус участника')
    points = models.PositiveSmallIntegerField(default=0, verbose_name='Очки участника')

    def __str__(self):
        return '{} {}'.format(self.second_name, self.first_name)


# Упражнения
class Exercise(models.Model):
    class Meta:
        verbose_name = 'Упражнение (Exercise)'
        verbose_name_plural = 'Список упражнений (Exercise)'

    points = models.PositiveSmallIntegerField(verbose_name='Кол-во очков')
    title = models.CharField(max_length=35, verbose_name='Название')

    def __str__(self):
        return '{} - {}'.format(self.points, self.title)


# Учёт выполненных упражнений (Персональная статистика)
class PersonalStat(models.Model):
    class Meta:
        verbose_name = 'Выполненное упражнение (PersonalStat)'
        verbose_name_plural = 'Выполненные упражнение (PersonalStat)'

    participant = models.ForeignKey(Participant, related_name='stats', verbose_name='Участник')
    exercise = models.ForeignKey(Exercise, verbose_name='Упражнение')
    time = models.DateTimeField(verbose_name='Время', default=datetime.datetime.now)
    operator = models.ForeignKey(User, verbose_name='Принимающий оператор')
    # Если параметр visible == False это говорит о удалении или редактировании данной записи

    def __str__(self):
        return '{} - {} ({})'.format(self.participant, self.exercise, self.time)